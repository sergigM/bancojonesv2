<?php require_once ('../helpers/i18n.php')?>
<html>
<link rel="stylesheet" href="../assets/css/global.css">
<body>

<div>
    <a href="profile.php">Profile</a>
    <a href="register.php">Register</a>
    <a href="welcome.php">Welcome</a>
    <a href="query.php">Transfer</a>
</div>

<?php require_once ('header.php')?>
<title><?php echo _("Registro");?></title>
<?php echo _('hola');
?>
<form action="../controller/controller.php" method="post">
    <ul>
        <p><?php echo _("Nombre");?></p>
        <input name="userName" type="text" placeholder="Rigberto" value='<?php echo$_POST['userName']?>'/>
    </ul>
    <ul>
        <p><?php echo _("Apellido");?></p>
        <input name="userSurn" type="text" placeholder="Garzia" value="<?php echo$_POST['userSurn']?>"/>
    </ul>
    <ul>
        <p><?php echo _("Fecha nacimiento");?></p>
        <input name="fechaNaci" type="date" value="<?php echo$_POST['fechaNaci']?>"/>
    </ul>
    <ul>
        <p><?php echo _("Genero");?></p>
        <select name="genero">
            <option>M</option>
            <option>F</option>
        </select>
    </ul>
    <ul>
        <p>DNI</p>
        <input name="dni" type="text" placeholder="00000000-A" value="<?php echo$_POST['dni']?>"/>
    </ul>
    <ul>
        <p><?php echo _("Telefono");?></p>
        <input name="tel" type="tel" placeholder="000000000" value="<?php echo$_POST['tel']?>"/>
    </ul>
    <ul>
        <p><?php echo _("Mail");?></p>
        <input name="mail" type="email" placeholder="ejemplo@ejemplo.com" value="<?php echo$_POST['mail']?>"/>
    </ul>
    <ul>
        <p><?php echo _("Contraseña");?></p>
        <input name="pass" type="password" placeholder="Password" />
    </ul>
    <ul>
        <p><?php echo _("Repetir Contraseña");?></p>
        <input name="passCopy" type="password" placeholder="Copy"/>
    </ul>

    <input name="control" value="register" type="hidden"/>

    <input name="submit" value="submit" type="submit" class="submit"/>
    <p class="error" ><?php
        if (isset($_POST['userName'])) {
            if (isset($_POST['Error'])) echo $_POST['Error'];
        }
        ?></p>
</form>

</body>

</html>
