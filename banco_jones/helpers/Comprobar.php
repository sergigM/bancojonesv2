<?php

function validate()
{
    $message = validateName($_POST['userName']) . validateSurn($_POST['userSurn']) . validateDate($_POST['fechaNaci']) . validateDni($_POST['dni']) . validateTel($_POST['tel']) . validateMail($_POST['mail']) . validatePass($_POST['pass'], $_POST['passCopy']);
    $_POST['Error']= $message;
    if ($message==null){
        return true;
    }else{
        return false;
    }
}

function validateProfile(){
    if ($_POST['mailP']==''){
        $_POST['mailP']=$_SESSION['mail'];
    }
    if ($_POST['telP']==''){
        $_POST['telP']=$_SESSION['tel'];
    }
    if ($_POST['passP']==''){
        $_POST['passP']=$_SESSION['pass'];
    }
    if ($_POST['passCP']==''){
        $_POST['passCP']=$_POST['passP'];
    }
    $message = validateTel($_POST['telP']) . validateMail($_POST['mailP']) . validatePass($_POST['passP'], $_POST['passCP']);
    $_POST['Error']= $message;
    if ($message==null){
        return [$_POST['telP'],$_POST['mailP'],$_POST['passP']];
    }else{
        return false;
    }
}

function validateName($userName)
{
    if ($userName == '') {
        return "Introdueix el teu nom. </br>";
    } elseif (preg_match('/[^a-z\s]/i', $userName)) {
        //nomes te lletres
        return "El teu nom no pot contenir numeros ni caracters especials. </br>";
    }
}

function validateSurn($userSur)
{
    if ($userSur == '') {
        return "Introdueix el teu cognom. </br>";
    } elseif (preg_match('/[^a-z\s]/i', $userSur)) {
        //nomes te lletres
        return 'El teu cognom no pot contenir numeros ni caracters especials. </br>';
    }
}

function validateDate($fechaNaci)
{
    $edad = date_diff(date_create($fechaNaci), date_create('today'))->y;
    if ($edad < 18) {
        return "No pots ser menor de 18 anys. </br>";
    }
}

function validateDni($dni)
{
    $partDni = explode("-", $dni);
    if ($dni == '') {
        return "Introdueix el teu DNI. </br>";
    } elseif (count($partDni) > 2 || count($partDni) == 1) {
        return "Introdueix correctament el teu DNI. </br>";
    } elseif (numDni($partDni) == false) {
        return "La lletra no correspon amb els numeros. </br>";
    }
}

function validateTel($tel)
{
    $numsTel = str_split($tel);
    if ($tel == '') {
        return "Introdueix el teu telefon. </br>";
    } elseif (count($numsTel) != 9 || $numsTel[0] != 6 && $numsTel[0] != 7) {
        return "Telefon no vàlid. </br>";
    }
}

function validateMail($mail)
{
    if ($mail == '') {
        return "Introdueix el teu mail. </br>";
    }
}

function validatePass($pass, $passCopy)
{
    $algo = ['0', '0', '0', '0'];//mayus, minus, num, special
    $longPass = str_split($pass);
    if ($longPass < 8) {
        return "La contrasenya ha de ser de més de 7 caracters. </br>";
    } elseif ($pass != $passCopy) {
        return "Contra diferent";
    } else {
        for ($i = 0; count($longPass) > $i; $i++) {
            if (preg_match('/[A-Z]/', $longPass[$i])) {
                $algo[0] = '1';
            }
            if (preg_match('/[a-z]/', $longPass[$i])) {
                $algo[1] = '1';
            }
            if (preg_match('/[0-9]/', $longPass[$i])) {
                $algo[2] = '1';
            }
            if (preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $longPass[$i])) {
                $algo[3] = '1';
            }
        }
        if ($algo[0] != 1 || $algo[1] != 1 || $algo[2] != 1 || $algo[3] != 1) {
            return "La contrasenya no cumpleix els requisists: Que contenga mayúsculas, minúsculas, números, un carácter especial. </br>";
        }
    }
}


function numDni($partDni)
{
    $numsDni = str_split($partDni[0]);
    if (count($numsDni) == 8) {
        if (letraDni($partDni) == true) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function letraDni($partDni)
{
    $lletra = ['T', 'R', 'W', 'A', 'G', 'H', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'];
    $result = $partDni[0] % 23;
    if ($lletra[$result] == strtoupper($partDni[1])) {
        return true;
    } else {
        return false;
    }
}
?>
