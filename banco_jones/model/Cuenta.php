<?php
class Cuenta
{


    private $saldo;
    private $id_cliente;
    private $iban;

    /**
     * Cuenta constructor.
     * @param $saldo
     * @param $id_cliente
     */
    public function __construct($saldo, $id_cliente,$iban)
    {
        $this->saldo = $saldo;
        $this->id_cliente = $id_cliente;
        $this->iban=$iban;
    }

    /**
     * @return mixed
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param mixed $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    /**
     * @param mixed $id_cliente
     */
    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }


}
?>