<?php
require_once('../helpers/DBManager.php');

use DBManager;
session_start();
function insertCliente($cliente)
{

    $manager = new DBManager();
    try {
        $sql = "INSERT INTO cliente (nombre, nacimiento, apellidos, dni, email,telefono, password,sexo) VALUES (:nombre,:fecha_nacimiento,:apellidos,:dni,:email,:telefono,:password,:sexo)";

        $password = password_hash($cliente->getPassword(), PASSWORD_DEFAULT, ['cost' => 10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':nombre', $cliente->getNombre());
        $stmt->bindParam(':fecha_nacimiento', $cliente->getFechaNacimiento());
        $stmt->bindParam(':apellidos', $cliente->getApellidos());
        $stmt->bindParam(':dni', $cliente->getDni());
        $stmt->bindParam(':sexo', $cliente->getSexo());
        $stmt->bindParam(':telefono', $cliente->getTelefono());
        $stmt->bindParam(':email', $cliente->getEmail());
        $stmt->bindParam(':password', $password);

        if ($stmt->execute()) {
            echo "todo OK";
        } else {
            echo "MAL";
        }

    } catch (PDOException $e) {
        echo $e->getMessage();
    }


}

function updateCliente($image){
    $manager = new DBManager();
    try{
        error_log('ClienteModel - updateCliente - INit UPDATE');
        $sql="UPDATE cliente SET imagen=:img WHERE dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':img',$image,PDO::PARAM_LOB);
        $stmt->bindParam(':dni', $_SESSION['dni'],PDO::PARAM_LOB);
        $stmt->execute();
        error_log('ClienteModel - updateCliente - Final UPDATE');
    }catch ( PDOException $e){
        echo $e->getMessage();
    }
}


//function updateClienteInfo($tel,$mail,$pass,$dni){
//    $manager = new DBManager();
//    try{
//        //error_log('ClienteModel - updateClienteInfo - INit UPDATE');
//        $sql="UPDATE cliente SET email=:mail,password=:passw,telefono=:tel WHERE dni=:dni";
//
//
//        //$pass = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 10]);
//
//
//
//        $stmt = $manager->getConexion()->prepare($sql);
//        $stmt->bindParam(':mail',$mail,PDO::PARAM_LOB);
//        $stmt->bindParam(':passw',$pass,PDO::PARAM_LOB);
//        $stmt->bindParam(':tel',$tel,PDO::PARAM_LOB);
//        $stmt->bindParam(':dni', $dni);
//
//
//        $stmt->execute();
//        //error_log('ClienteModel - updateClienteInfo - Final UPDATE');
//    }catch ( PDOException $e){
//        echo $e->getMessage();
//    }
//
//}

function updateClienteTel($tel,$dni){
    $manager = new DBManager();
    try{
        //error_log('ClienteModel - updateClienteInfo - INit UPDATE');
        $sql="UPDATE cliente SET telefono=:tel WHERE dni=:dni";
        error_log('ClienteModel - updateClienteTel - INit UPDATE');

        //$pass = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':tel',$tel,PDO::PARAM_LOB);
        $stmt->bindParam(':dni', $dni);


        $stmt->execute();
        //error_log('ClienteModel - updateClienteInfo - Final UPDATE');
    }catch ( PDOException $e){
        echo $e->getMessage();
    }
}

function updateClienteMail($mail,$dni){
    $manager = new DBManager();
    try{
        //error_log('ClienteModel - updateClienteInfo - INit UPDATE');
        $sql="UPDATE cliente SET email=:mail WHERE dni=:dni";

        error_log('ClienteModel - updateClienteMail - INit UPDATE');
        //$pass = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 10]);
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':mail',$mail,PDO::PARAM_LOB);

        $stmt->bindParam(':dni', $dni);


        $stmt->execute();
        //error_log('ClienteModel - updateClienteInfo - Final UPDATE');
    }catch ( PDOException $e){
        echo $e->getMessage();
    }

}


function updateClientePass($pass,$dni){
    $manager = new DBManager();
    try{
        error_log('ClienteModel - updateClientePass - INit UPDATE');

        //error_log('ClienteModel - updateClienteInfo - INit UPDATE');
        $sql="UPDATE cliente SET password=:passw WHERE dni=:dni";


        //$pass = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 10]);
        $stmt = $manager->getConexion()->prepare($sql);

        $stmt->bindParam(':passw',$pass,PDO::PARAM_LOB);

        $stmt->bindParam(':dni', $dni);


        $stmt->execute();
        //error_log('ClienteModel - updateClienteInfo - Final UPDATE');
    }catch ( PDOException $e){
        echo $e->getMessage();
    }

}








function selectCliente($dni)
{
    $manager = new DBManager();
    try {
        $sql = "Select * From cliente WHERE dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':dni', $dni);
        $stmt->execute();
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);
        $obj= new Cliente($result[0]['nombre'],$result[0]['nacimiento'],$result[0]['apellidos'],$result[0]['dni'],$result[0]['email'],$result[0]['telefono'],$result[0]['password'],$result[0]['sexo'],$result[0]['imagen']);
        return $obj;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }


}

function getUserHash($dni)
{
    $conexion = new DBManager();
    try {
        $sql = "SELECT password FROM cliente WHERE dni=:dni";
        $stmt = $conexion->getConexion()->prepare($sql);
        $stmt->bindParam(':dni', $dni);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['password'];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

?>