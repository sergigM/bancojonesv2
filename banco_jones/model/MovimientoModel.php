<?php
function existeCuenta($iban){
    //error_log('----------------------'.$iban);
    $manager = new DBManager();
    try {
        $sql = "SELECT * FROM cuenta WHERE iban=:iban";
        //error_log('*********************'.$iban);
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':iban',$iban);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $manager->cerrarConexion();

        if (count($rt)>0){
            return true;
        }else{
            return false;
        }

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}

function transfer($id_origen, $id_destino, $cantidad)
{

    $manager = new DBManager();
    try {
        $sql = "INSERT INTO movimientos (fecha,cantidad,id_origen,id_destino) VALUES (now(),:cantidad,:origen,:destino)";
        $stmt = $manager->getConexion()->prepare($sql);
        //error_log("--- " . $id_origen  . $id_destino .  $cantidad);

        $stmt->bindParam(':origen', $id_origen);
        $stmt->bindParam(':destino', $id_destino);
        $stmt->bindParam(':cantidad', $cantidad);
        $stmt->execute();

        $sql = "UPDATE cuenta SET saldo = saldo - $cantidad WHERE iban=:origen";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':origen', $id_origen);
        $stmt->execute();

        $sql = "UPDATE cuenta SET saldo = saldo + $cantidad WHERE iban=:destino";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':destino', $id_destino);
        $stmt->execute();


        $manager->cerrarConexion();

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}




?>