<?php
require('../model/Cliente.php');
require('../model/ClientModel.php');

require('../model/Cuenta.php');
require('../model/CuentaModel.php');

require('../model/MovimientoModel.php');

use Cliente;

require_once('../helpers/Comprobar.php');

if ($_POST['switch_lang'] == 'switch_lang') {
    $lang = $_POST['lang'];
    setcookie('lang', $lang, time() + 60 * 60 * 24 * 34, '/', 'localhost');
    header('Location:', $_SERVER['HTTP_REFERER']);
    header("Location: ../views/register.php");
}

if (isset($_POST['submit'])) {
    if ($_POST['control'] == 'register') {
        if (validate()) {
            $cliente = new Cliente($_POST['userName'], $_POST['fechaNaci'], $_POST['userSurn'], $_POST['dni'], $_POST['mail'], $_POST['tel'], $_POST['pass'], $_POST['genero'], "");
            insertCliente($cliente);

            header("Location: ../views/login.php");
        }
        require_once('../views/register.php');
    } elseif ($_POST['control'] == 'login') {

        $hash = getUserHash($_POST['dni']);
        if (password_verify($_POST['pass'], $hash)) {
            session_abort();
            session_start();
            $obj = selectCliente($_POST['dni']);
            $_SESSION['userName'] = $obj->getNombre();
            $_SESSION['userSurn'] = $obj->getApellidos();
            $_SESSION['nacimiento'] = $obj->getFechaNacimiento();
            $_SESSION['sexo'] = $obj->getSexo();
            $_SESSION['tel'] = $obj->getTelefono();
            $_SESSION['mail'] = $obj->getEmail();
            $_SESSION['dni'] = $obj->getDni();
            $_SESSION['pass'] = $obj->getPassword();

            ob_start();
            fpassthru($obj->getImg());
            $data = ob_get_contents();
            ob_end_clean();

            $img = "data:image/*;base64," . base64_encode($data);
            $foto = '<img src="' . $img . '" width="50%" height="50%"/>';
            $_SESSION['foto'] = $foto;


            header('Location: ../views/welcome.php');
        } else {
            $_POST['Error'] = "Error";
            require_once('../views/login.php');
        }

    } elseif ($_POST['control'] == 'profile') {
        $obj = selectCliente($_SESSION['dni']);

        $check = getimagesize($_FILES['upload']['tmp_name']);


        $fileName = $_FILES['upload']['name'];
        $fileSize = $_FILES['upload']['size'];
        $fileType = $_FILES['upload']['type'];
        echo $fileName . "</br>";
        echo $fileSize . "</br>";
        echo $fileType . "</br>";


        if (!$_POST['mailP'] == "") {
            if (validateMail($_POST['mailP']) == "") {

                updateClienteMail($_POST['mailP'], $_SESSION['dni']);


            } else {
                $_POST['Error'] = $_POST['Error'] . validateMail($_POST['mailP']);
            }
        }
        if (!$_POST['passP'] == "" || !$_POST['passCP'] == "") {

            if (validatePass($_POST['passP'], $_POST['passCP']) == "") {

                updateClientePass($_POST['passP'], $_SESSION['dni']);

            } else {
                $_POST['Error'] = $_POST['Error'] . validatePass($_POST['passP'], $_POST['passCP']);

            }
        }


        if (!$_POST['telP'] == "") {


            if (validateTel($_POST['telP']) == "") {

                updateClienteTel($_POST['telP'], $_SESSION['dni']);
            } else {
                $_POST['Error'] = $_POST['Error'] . validateTel($_POST['telP']);

            }

        }


        if ($check !== false) {
            error_log('Controller - profile - check image');
            $img = file_get_contents($_FILES['upload']['tmp_name']);
            updateCliente($img);
        }

        ob_start();
        fpassthru($obj->getImg());
        $data = ob_get_contents();
        ob_end_clean();
        $_SESSION['foto'] = $obj->getImg();
        require_once('../views/profile.php');
        $img = "data:image/*;base64," . base64_encode($data);


        $_SESSION['tel'] = $obj->getTelefono();
        $_SESSION['mail'] = $obj->getEmail();
        $_SESSION['foto'] = $obj->getImg();


    }
    if ($_POST['control'] == 'create') {
        createAccount($_SESSION['dni']);
        header("Location: ../views/welcome.php");

    }
    if ($_POST['control'] == 'select_account') {
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['cuenta'] = $_POST['cuentas'];
        $_SESSION['saldo'] = $saldo;
        header("Location: ../views/query.php");
    }
    if ($_POST['control'] == 'transfer') {
        error_log($_POST['cuenta_destino']);
        if (existeCuenta($_POST['cuenta_destino'])) {
            transfer($_SESSION['cuenta'], $_POST['cuenta_destino'], $_POST['cantidad']);
            header("Location: ../views/welcome.php");

        }
    }


}

?>