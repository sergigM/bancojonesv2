<?php

// Ex 1


class Contador
{
    private $cont;

    function __construct()
    {
        $params = func_get_args();
        $num_params = func_num_args();
        $funcion_constructor = '__construct' . $num_params;
        if (method_exists($this, $funcion_constructor)) {
            call_user_func_array(array($this, $funcion_constructor), $params);
        }
    }

    function __construct1($cont)
    {
        $this->setContador($cont);

    }

    function setContador($cont)
    {
        $this->cont = $cont;

    }

    function getContador()
    {
        return $this->cont;
    }

    function incrementar()
    {
        $cont=$this->getContador();
        $this->setContador($cont+1);
    }

    function decrementar()
    {
        $cont=$this->getContador();
        $this->setContador($cont-1);
    }
}


//Ex 2

class Libro
{
    private $prestamo = true;
    private $devolucion = true;


    public function __construct()
    {
        $params = func_get_args();
        $num_params = func_num_args();
        $funcion_constructor = '__construct' . $num_params;
        if (method_exists($this, $funcion_constructor)) {
            call_user_func_array(array($this, $funcion_constructor), $params);
        }
    }

    public function __construct1()
    {

    }

    public function __construct2($prestamo, $devolucion)
    {
        $this->setPrestamo($prestamo);
        $this->setDevolucion($devolucion);
    }

    function setPrestamo($prestamo)
    {
        $this->prestamo = $prestamo;

    }

    function getPrestamo()
    {
        return $this->prestamo;
    }

    function setDevolucion($devolucion)
    {
        $this->devolucion = $devolucion;

    }

    function getDevolucion()
    {
        return $this->devolucion;
    }

    function mostrar()
    {
        echo $this->getDevolucion()."</br>";
        echo $this->getPrestamo()."</br>";
    }
}

$algo = new Libro(false,true);
echo $algo->mostrar();
























