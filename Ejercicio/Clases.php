<?php

class MiClase
{
    public $publico = "Publico";
    protected $protected = "Protected";
    private $private = "private";

    public function __construct()
    {

    }

    function printAtr()
    {
        echo $this->private . "<br/>";
        echo $this->protected . "<br/>";
        echo $this->publico . "<br/>";
    }
}


$obj = new MiClase();
echo $obj->publico . "<br/>";
echo $obj->printAtr();


class MiClass2 extends MiClase
{
    public $publico = "Publico 2";
    protected $protected = "Protected 2";

    public function __construct()
    {
        parent::__construct();
    }


    function printAtr()
    {
        //echo $this->private."<br/>";
        echo $this->protected . "<br/>";
        echo $this->publico . "<br/>";
    }
}

$obj = new MiClass2();
echo $obj->publico . "<br/>";
echo $obj->printAtr();


abstract class Animal
{
    abstract function sonido();

}

class Gato extends animal
{
    public function sonido()
    {
        // TODO: Implement sonido() method.
        echo "Miau";
    }
}

class Perro extends animal
{
    public function sonido()
    {
        // TODO: Implement sonido() method.
        echo "Guau";
    }
}

$gato = new Gato();
$perro = new Perro();
$gato->sonido();
$perro->sonido();


interface Interfaz{
    public function metodoUno($atr);
}

class Clase implements Interfaz{

    public function metodoUno($atr)
    {
        // TODO: Implement metodoUno() method.

        echo $atr;
    }
}
$algo=new Clase();
$algo->metodoUno("aaaa");

























